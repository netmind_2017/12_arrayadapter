package clase21012017.holamundo21;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import clase21012017.holamundo21.model.Animal;

public class Details extends AppCompatActivity {

    TextView label;
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animaldetails);

        label = (TextView) findViewById(R.id.textView);
        image = (ImageView) findViewById(R.id.imageView);

        Animal item = (Animal) getIntent().getSerializableExtra("param");

        label.setText(item.name);
        image.setImageResource(item.imagepictures);

    }
}
