package clase21012017.holamundo21.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import clase21012017.holamundo21.R;
import clase21012017.holamundo21.model.Animal;

public class AnimalAdapter extends ArrayAdapter<Animal> {

    private Context context;
    private List<Animal> items;

    public AnimalAdapter(Context context, ArrayList<Animal> items) {
        super(context, 0, items);
        this.context = context;
        this.items = items;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.list_animal, parent, false);
        }

        // findViewByIds
        ImageView animal_Image = (ImageView) rowView.findViewById(R.id.animal_Image);
        TextView animal_Title = (TextView) rowView.findViewById(R.id.animal_Title);


        /* ROW ITEM */
        Animal item = this.items.get(position);
        animal_Title.setText(item.id +  " " + item.name);
        animal_Image.setImageResource(item.imagepictures);

        return rowView;

    }
}
