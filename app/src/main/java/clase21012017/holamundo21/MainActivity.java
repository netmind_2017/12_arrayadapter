package clase21012017.holamundo21;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;
import java.util.ArrayList;

import clase21012017.holamundo21.adapter.AnimalAdapter;
import clase21012017.holamundo21.model.Animal;

public class MainActivity extends AppCompatActivity /*implements AdapterView.OnItemSelectedListener*/ {

    Spinner spinnerCoche;
    ListView list;
    public ArrayList<Animal> items;
    AnimalAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        //FINDVIEWBYIds
        list = (ListView) findViewById(R.id.lista);

        items = new ArrayList<Animal>();
        items.add(new Animal(1,"Tom ",R.drawable.pic_1));
        items.add(new Animal(2,"John",R.drawable.pic_3));
        items.add(new Animal(3,"Black",R.drawable.pic_5));
        items.add(new Animal(4,"Adan",R.drawable.pic_1));
        items.add(new Animal(5,"Alvin",R.drawable.pic_3));
        items.add(new Animal(6,"Andrew",R.drawable.pic_5));
        items.add(new Animal(7, "Arin",R.drawable.pic_1));
        items.add(new Animal(8,"Angus",R.drawable.pic_3));
        items.add(new Animal(9,"Arnus",R.drawable.pic_5));
        items.add(new Animal(10,"Anderson",R.drawable.pic_1));
        items.add(new Animal(11, "Allen",R.drawable.pic_3));
        items.add(new Animal(12, "Alexander",R.drawable.pic_5));
        items.add(new Animal(13, "Adrian",R.drawable.pic_1));
        items.add(new Animal(14, "Arnus",R.drawable.pic_3));
        items.add(new Animal(15, "Andrew ",R.drawable.pic_5));
        items.add(new Animal(16, "Peter ",R.drawable.pic_1));
        items.add(new Animal(17, "Holand ",R.drawable.pic_5));

        //ADAPTER
        adapter = new AnimalAdapter(getApplicationContext(),items);
        list.setAdapter(adapter);

        //ON CLICK LISTENER
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view,int position, long arg) {

                Animal item = (Animal) list.getAdapter().getItem(position);
                Log.d("MainActivity: ",item.name);

                Intent i = new Intent(getApplicationContext(), Details.class);
                i.putExtra("param",item);
                startActivity(i);
            }
        });

    }

}
